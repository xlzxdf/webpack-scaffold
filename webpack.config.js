const path = require('path')
const { VueLoaderPlugin } = require('vue-loader')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
module.exports = {
    //入口文件
    entry: "./src/main.js",
    //环境配置
    //  mode:"development",
    //打包输出文件
    output: {
        path: path.resolve(__dirname, 'dist'),
        //打包后的文件名
        filename: 'bundle.js',

    },
    //loader配置项
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: 'vue-loader'
            },
            {
                test: /\.css/,
                use: [MiniCssExtractPlugin.loader, 'css-loader']
            }
        ]
    },
    //插件配置项
    plugins: [
        new VueLoaderPlugin(),
        new MiniCssExtractPlugin({
            filename: 'css/[name].css'
            // 输出的css文件名不变的意思
        })
    ],


    //服务器配置
    devServer: {

    }
}
